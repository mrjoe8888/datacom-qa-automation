# QA Datacom Automation

Welcome to QA Datacom Automation.

## Folder Structure

- e2e - contains Functional test cases 

- plugins - deals with operations outside the application under tests 

- support - contains the custom commands and function libraries to support the automation

## Installation

- from the command(windows)/shell(linux), send the command "npm install". This will install all dependencies including the cypress. for references of the needed items to install are mentioned in package.json

## Requirements

As for those who wishes to use this automation, the IDE that we are using is Visual Studio Code.

- [GIT] - a free and open source distributed version control system. you need to install and configure to connect to github

- [Visual Studio] - A text editor suitable for any javascript coding
  Link : https://code.visualstudio.com/
  Installer: https://code.visualstudio.com/

++ [Extensions] - Once you have installed the VS Code, from the left navigation bar, open the extension icon (box icon) and it will show the list of the extensions. Please install the following
Beautify - This is to beautify the code
GitHub - Connects to GitHub repository
TODO Highlight - This will help you to comment the codes that are yet to be done

++ [VS Code Settings] - To change the settings on the VS Code, I would recommend to change the following settings
From File > Preferences > Settings, Navigate to Text Editor > Formatting and check the following options
- Format on Paste - automatically formats once you paste the code copied from other scripts
- Format on Save - automatically formats the code once you save the file

## Post-Requisite

After the cloning of this repository, please run "npm install" on windows and "sudo npm install" on linux / mac to install the dependencies.

## Running
To run the application, there are few ways to perform.

For UI view, just send the command 
"npm run cy:run". 
For details on the script, please check the package.json on script section.

For headless view,
- Run all specs  
   "npx cypress run"
- Run UI only
   "npx cypress run ./cypress/e2e/ui/*"
- Run API only
   "npx cypress run ./cypress/e2e/api/*"

With specific browser,
- Firefox  
   "npx cypress run --browser firefox"
- Chrome 
   "npx cypress run --browser chrome"
- Microsoft Edge 
   "npx cypress run --browser edge"
- Electron 
   "npx cypress run"


With parallel execution,
send the command
"npm run cy:parallel-regression"


## Test cases Creation

- For UI
Application URL: https://www.demo.bnz.co.nz/client/

TC1: Verify you can navigate to Payees page using the navigation menu
1. Click ‘Menu’
2. Click ‘Payees’
3. Verify Payees page is loaded

TC2: Verify you can add new payee in the Payees page
1. Navigate to Payees page
2. Click ‘Add’ button
3. Enter the payee details (name, account number)
4. Click ‘Add’ button
5. ‘Payee added’ message is displayed, and payee is added in the list of payees


TC3: Verify payee name is a required field
1. Navigate to Payees page
2. Click ‘Add’ button
3. Click ‘Add’ button
4. Validate errors
5. Populate mandatory fields
6. Validate errors are gone

TC4: Verify that payees can be sorted by name
1. Navigate to Payees page
2. Add new payee
3. Verify list is sorted in ascending order by default
4. Click Name header
5. Verify list is sorted in descending order

TC5: Navigate to Payments page
1. Navigate to Payments page
2. Transfer $500 from Everyday account to Bills account
3. Transfer successful message is displayed
4. Verify the current balance of Everyday account and Bills account are correct

Note: Run this test 3 times to ensure 100% pass rate

Bonus:
1. Run the tests within a continuous integration pipeline 
2. Run the tests in docker
3. Run the tests in different browsers
4. Run the tests in parallel


- For API
Using any API open-source test tool of your choice, automate the following: 
Request URL: https://jsonplaceholder.typicode.com/users

TC1: Verify GET Users request
1. Verify 200 OK message is returned
2. Verify that there are 10 users in the results

TC2: Verify GET User request by Id
1. Verify 200 OK message is returned
2. Verify if user with id8 is Nicholas Runolfsdottir V

TC3: Verify POST Users request
1. Verify 201 Created message is returned
2. Verify that the posted data are showing up in the result

Bonus:
1. Parameterised your tests
2. Run the tests within a continuous integration pipeline
