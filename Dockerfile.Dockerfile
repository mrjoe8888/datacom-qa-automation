FROM cypress/browsers:node16.14.2-slim-chrome100-ff99-edge

WORKDIR /cypress-src
COPY package.json /cypress-src
COPY package-lock.json /cypress-src

RUN npm i
RUN npx cypress verify
RUN npx cypress run