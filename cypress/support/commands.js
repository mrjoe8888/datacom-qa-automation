/* Creator: Joseph ocero
 * @LastEditors: 
 * @DateCreated: 2022-09-01 
 * @LastEditTime: 2022-09-04
 * @Description: Adding API custom methods, minor enhancement to the code
 **/

import { Actions, Locators } from "./config";
import { getLocatorContains } from "./common";

/*
* Demo API testing
*/
Cypress.Commands.add('apiRequest', (method = 'GET', params = {}, payload = {}) => {
  var dataRequest = {
    method: method,
  }
  console.log(Object.keys(params).length)
  if (Object.keys(params).length != 0){
      dataRequest['url'] = `${Cypress.env('backendUrl')}/users?id=${params.id}`
  }else{
    dataRequest['url'] = `${Cypress.env('backendUrl')}/users`
  }
  if (Object.keys(payload).length != 0){
    dataRequest = { ...dataRequest, body: payload,  
    }
  }
  cy.request(dataRequest)
})

/*
* Demo UI testing navigation to Payee's menu and click add new payee
*/
Cypress.Commands.add('navNewPayee', () => {
  Actions.click(getLocatorContains('button', 'Menu'))
  Actions.click(getLocatorContains('span', 'Payees'))
  Actions.click(getLocatorContains('span', 'Add'))
})

/*
* Demo UI testing data entry with adding new payee in two dimensional array
* [[locator1, inputvalue1], [locatorNth, inputValueNth]]
*/
Cypress.Commands.add('dataEntry', (payeedata) => {
  for (let [locator, inputvalue] of payeedata) {
    Actions.type(locator, inputvalue)
  }
});



