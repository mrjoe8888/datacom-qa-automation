/*
 * @Author: Joseph Ocero
 * @Date: 2021-09-04 
 * @LastEditors: Joseph Ocero
 * @LastEditTime:
 * @Description: file content
 */
import { faker } from '@faker-js/faker';
import * as Const_App from "./constants/app";

//json

import Message from "./json/ui/message.json";
import Locators from "./json/ui/locators.json";
import URL from "./json/ui/url.json";

//libraries
import Actions from "./lib/ui/actions";

export {
	Const_App,
	faker, //all constants
	Locators,
	Message, //all json
	URL,
	Actions,
};
