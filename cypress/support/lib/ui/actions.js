/*
 * @Date: 2022-09-04 
 * @LastEditors: Joseph Ocero
 * @LastEditTime: 
 * @Description: Contains common page events or actions
 * @History: 
 */

import { Menu, Page, Table, URL, Const_App } from "../../config";


class Actions {
	/**
	 * Checks the element if visible and type
	 *
	 * @param  {string} element
	 * locator of the element
	 *
	 * @param  {string} text
	 * text to pass
	 *
	 * @param {number} nth
	 * specify if multiple elements
	 *
	 */
	static type(element, text, nth = 0) {
		cy.get(element)
			.eq(nth)
			.clear()
			.type(`${text}{enter}`)
	}
	
	/**
	 * Checks the element if visible and clicks
	 *
	 * @param  {string} element
	 *  locator if the element
	 *
	 * @param {number} index
	 * order of elements in page if multiple elements exists
	 */
	static click(element, index = 0) {
		cy.get(element, {
			timeout: 10000,
		}).eq(index)
		  .should("be.visible", {
				timeout: 6000,
		  })
		.click();
	}


	/**
	 * Selects the element from the dropdown list
	 *
	 * @param  {string} element
	 *  locator of element
	 *
	 * @param  {string} text
	 * text to pass
	 *
	 * @param {number} nth
	 * specify if multiple elements
	 * @param  {string} value
	 * value to compare from selected text
	 *
	 */
	static select(element, text, value, nth = 0) {
		cy.get(element).eq(nth).select(text, {
			timeout: 10000,
		}).should('have.value', value)
	}

		
	/**
	 * Checks if the message is displayed
	 *
	 * @param  {string} text
	 * text to pass
	 * @param  {string} element
	 *  locator of element
	 */
	static checkMessage(element, text) {
		cy.get(element, {
			timeout: 60000,
		})
			.should("be.visible")
			.should("contain", text);
	}

	/**
	 * verify the banner message
	 *
	 * @param  {string} element
	 *  locator of element
	 *
	 * @param  {string} message
	 * text message to pass
	 *
	 */
	static checkString(element, message) {
		cy.get(element, {
			timeout: 10000,
		}).invoke("text").should("be.eq", message)	
	}

	/**
	 * verify the Attribute value
	 *
	 * @param  {string} element
	 *  locator of element
	 *
	 * @param  {string} attribute
	 * attribute to pass
	 *
	 * @param  {string} text
	 * text value to pass
	 *
	 * @param  {string} condition
	 * condition value to pass: "included", "not.contain"
	 * 
	 * @param  {string} attrtype
	 * attribute type to pass: "class", "aria-label"
	 */
	 static checkAttribute(element, condition, text, attrtype='class') {
		cy.get(element, {
			timeout: 10000,
		}) 
		.invoke('attr', attrtype)
		.should(condition, text)	
	}
}
export default Actions;
