/*
 * @Date: 2019-09-04 
 * @LastEditors: Joseph Ocero
 * @LastEditTime: 
 * @Description: file content
 */
import { faker } from '@faker-js/faker';

// ****************** PAYEE DATA *************** //
export const PAYEE_DATA = {
	payeeName: `${faker.name.firstName()} ${faker.name.lastName()}`,
    bankName: Math.floor(Math.random()*(99-9+1)+9),
    branchName: Math.floor(Math.random()*(9999-1000+1)+1000),
    account: Math.floor(Math.random()*(9999999-1000000+1)+1000000),
    suffix: Math.floor(Math.random()*(99-9+1)+9),
};

export const API_TYPICODE_PAYLOAD = {
	name: faker.name.findName(),
	username: faker.internet.userName(),
	email: faker.internet.email(),
	address:{
	street: faker.address.streetAddress(),
	suite:`Suite ${Math.random().toString(8).substr(2, 8)}`,
	city: faker.address.city(),
	zipcode: faker.address.zipCode().toString(8),
	geo: {
		lat: faker.address.latitude(),
		lng: faker.address.longitude()
	}
	},
	phone: faker.phone.phoneNumber(), 
	website: faker.internet.domainName(),
	company:{
	name: faker.company.companyName(),
	catchPhrase: faker.random.words(8),
	bs: faker.random.words(3)
	},
	id: faker.datatype.number()
};
