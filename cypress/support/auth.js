/* Creator: Joseph ocero
 * @LastEditors: 
 * @DateCreated: 2022-09-02 
 * @LastEditTime: 2022-09-04
 * @Description: basic Auth login
 **/
/// <reference types="cypress" />

export const navToPage = (menu) => {
    return cy.visit(`/${menu}/`, { 
        headers: {
        "Accept-Encoding": "gzip, deflate"
      }
    })      
}