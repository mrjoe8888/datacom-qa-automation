/* Creator: Joseph ocero
 * @LastEditors: 
 * @DateCreated: 2022-09-04 
 * @LastEditTime: 
 * @Description: custom methods
 **/
export const getDateNextMonday = () => {
    var days =[1,7,6,5,4,3,2];
    var dateOnMonday = new Date();
    dateOnMonday.setDate(dateOnMonday.getDate()+days[dateOnMonday.getDay()]);
    dateOnMonday = dateOnMonday.toISOString().split('T')[0].split('-').reverse().toString()
    dateOnMonday = dateOnMonday.replace(/,/g, "/");
    return dateOnMonday
}

export const getLocatorContains = (attribute, strsearch) => {
    return `${attribute}:contains('${strsearch}')`
}