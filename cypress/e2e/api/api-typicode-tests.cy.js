/* Creator: Joseph ocero
 * @LastEditors: 
 * @DateCreated: 2022-09-01
 * @LastEditTime: 
 * @Description: API Testing
 **/
/// <reference types="cypress" />

import { Const_App } from "../../support/config";

describe("API services testing", function() { 
    var method, 
    params,
    name = "Nicholas Runolfsdottir V";
    it('TC1: Verify GET Users request', () => {
        cy.apiRequest(method = 'GET', params = {}, Const_App.API_TYPICODE_PAYLOAD).then((response) => {
            const numUsers = (JSON.stringify(response.body).split("username").length - 1);
            expect(response.status).to.be.equal(200);
            expect(response.body.length).to.be.equal(10);
            expect(numUsers).to.be.equal(10)
        })
    })
    it('TC2: Verify GET User request by Id', () => {
        cy.apiRequest(method = 'GET', params = {id: 8}).then((response) => {
            expect(response.status).to.be.equal(200);
            expect(response.body[0].name).to.be.equal(name)
        })
    })
    it('TC3: Verify POST Users request', () => {
        cy.apiRequest(method = 'POST', params = {}, Const_App.API_TYPICODE_PAYLOAD).then((response) => {
            expect(response.status).to.be.equal(201);
            Const_App.API_TYPICODE_PAYLOAD['id'] = response.body.id;
            expect(JSON.stringify(Const_App.API_TYPICODE_PAYLOAD)).to.be.equal(JSON.stringify(response.body))
        })
    })
})