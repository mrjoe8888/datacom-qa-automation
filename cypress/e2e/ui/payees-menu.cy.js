/* Creator: Joseph ocero
 * @LastEditors: 
 * @DateCreated: 2022-09-01
 * @LastEditTime: 2022-09-05
 * @Description: Use cases scenarios for payee, 
 * adding messages and locators data, updating action class
 **/
/// <reference types="cypress" />

import { navToPage } from "../../support/auth";
import { getDateNextMonday, getLocatorContains } from "../../support/common";
import { URL, Const_App, Actions, Locators, Message } from "../../support/config";

describe("Payees' menu", function() { 
    var payeeName, bankName, branchName, account, suffix, payeeData = [];
    
    beforeEach(() => {
        navToPage(URL['client.menu'])
        payeeName = Const_App.PAYEE_DATA['payeeName'];
        bankName = Const_App.PAYEE_DATA['bankName'];
        branchName = Const_App.PAYEE_DATA['branchName'];
        account = Const_App.PAYEE_DATA['account']
        suffix = Const_App.PAYEE_DATA['suffix'];
        payeeData = [
            [Locators.payee.name, payeeName],
            [Locators.payee.bankname, bankName],
            [Locators.payee.branchname, branchName],
            [Locators.payee.account, account],
            [Locators.payee.suffix, suffix]
        ]
    })

    it("TC1: Verify you can navigate to Payees page using the navigation menu", () => { 
        Actions.click(getLocatorContains('button', 'Menu'))
        Actions.click(getLocatorContains('span', 'Payees'))
        Actions.checkString(getLocatorContains('span', 'Payees'), "Payees")
        cy.url().should("include", `${Cypress.config().baseUrl}/${URL["client.payees"]}`);
       
    })
    it("TC2: Verify you can add new payee in the Payees page", () => { 
        cy.navNewPayee()
        cy.dataEntry(payeeData)
        Actions.click(Locators['payee.button'].submit)
        Actions.checkString(getLocatorContains('span', 'Payee added'), Message['payee.message'].addpayee)
        cy.contains("span[class='js-payee-name']", payeeName).invoke("text").should("be.eq", payeeName)
    })
    it("TC3: Verify payee name is a required field", () => { 
        cy.navNewPayee()

        Actions.click(Locators['payee.button'].submit)
        var attrtype = "aria-label";
        Actions.checkAttribute(Locators.payee.name, 'include',  Message['payee.message'].payeename_error, attrtype)
        cy.get(Locators.payee.errorBox).invoke("text").should("contain", Message['payee.message'].payeename_alert)
        
        cy.dataEntry(payeeData)
        Actions.checkAttribute(Locators['payee.button'].submit, "not.contain", "disabled")
        cy.contains(Message['payee.message'].payeename_error).should('not.exist')
        cy.contains(Message['payee.message'].payeename_alert).should('not.exist')
        
    })
    it("TC4: Verify that payees can be sorted by name", () => { 
        cy.navNewPayee()
        cy.dataEntry(payeeData)
        Actions.click(Locators['payee.button'].submit)
        Actions.checkAttribute(Locators.payee.sortBtn, "include", "Icon IconChevronDownSolid ")
        Actions.click(Locators.payee.sortBtn)
        Actions.checkAttribute(Locators.payee.sortBtn, "include", "Icon IconChevronUpSolid ")

    })
    it("TC5: Navigate to Payments page", () => { 
        Actions.click(getLocatorContains('button', 'Menu'))
        Actions.click(getLocatorContains('span', 'Pay or transfer'))
        Actions.click(Locators['payment.button'].from_account_user)
        Actions.click(getLocatorContains('p', 'Everyday'))
        
        cy.get(Locators.payments.currBalance.replace("$nth", 1)).invoke("text").then((currBal) => {
           const fromCurrBal = currBal.split(' ')[0]
           Actions.click(Locators['payment.button'].to_account_user)
            Actions.type(Locators['payments'].searchuser, 'Bills')
            Actions.click(getLocatorContains('p', 'Bills '))
            cy.get(Locators.payments.currBalance.replace("$nth", 2)).invoke("text").then((currBal) => {
                const toCurrBal = currBal.split(' ')[0]
                cy.get(Locators['payments'].amount).type(500)
                const element = Locators['payments'].frequency, 
                    text = "Daily", 
                    value = text.toUpperCase();
                Actions.select(element, text, value)
                Actions.click(Locators['payments'].dateformat)
                Actions.type(Locators['payments'].date, getDateNextMonday())
               
                Actions.click(Locators['payment.button'].submit)
                Actions.click(getLocatorContains('button', 'Confirm'))
                Actions.checkString(Locators['payment.message'].auto_payment_created, Message['payment.message'].auto_payment_created)
                navToPage(URL['client.menu'])
                Actions.click(getLocatorContains('h3', 'Everyday'))
                cy.get(Locators.payments.accntEveryday_currBal).invoke("text").then((currBal) => {
                    expect(`$${currBal}`).to.be.equal(fromCurrBal)
                })
                Actions.click(Locators['payment.button'].exit) //click 'x' button
                Actions.click(getLocatorContains('h3', 'Bills'))
                cy.get(Locators.payments.accntBills_currBal).invoke("text").then((currBal) => {
                    expect(`$${currBal}`).to.be.equal(toCurrBal)
                })
                
            })
        })
    })
})




